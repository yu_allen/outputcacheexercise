﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tryOutputCatch.Controllers
{
    public class ParamSampleController : Controller
    {
        //在設定後發現將varyByParam參數設定在web.config疑似會有參考以下網址
        //http://stackoverflow.com/questions/23764926/asp-net-outputcache-varybyparam-does-not-work-in-web-config
        //[OutputCache(CacheProfile = "AllPassParam")]
        //所以改成以下做法
        [OutputCache(CacheProfile = "AllPassParam",VaryByParam ="*")]
        public ActionResult Index(string x,string y, string z)
        {
            ViewBag.Subject = "不限制參數";
            ViewBag.Description = "只要參數不同或是參數的值不同，就會建立一版不同的cache";
            ViewBag.eg = string.Format("代入的參數值為:{0},{1},{2}",x,y,z);
            ViewBag.CacheTime = DateTime.Now;
            return View();
        }

        //[OutputCache(CacheProfile = "SpecifyParam")]
        [OutputCache(CacheProfile = "SpecifyParam",VaryByParam = "myId;myName")]
        public ActionResult Index2(string myId, string myName, string myAge)
        {
            ViewBag.Subject = "指定參數";
            ViewBag.Description = "本範例設定為只有參數myId及myName的值不同時會建立不同版本的cache，參數myAge不會";
            ViewBag.eg = string.Format("代入的參數值為:{0},{1},{2}", myId, myName, myAge);
            ViewBag.CacheTime = DateTime.Now;
            return View("Index");
        }

        //[OutputCache(CacheProfile = "NotAllowParam")]
        [OutputCache(CacheProfile = "NotAllowParam",VaryByParam ="none")]
        public ActionResult Index3(string myId, string myName, string myAge)
        {
            ViewBag.Subject = "不允許參數";
            ViewBag.Description = "本範例設定為只有一個版本的cache，參數的值不同並不會建立不同版本的cache";
            ViewBag.eg = string.Format("代入的參數值為:{0},{1},{2}", myId, myName, myAge);
            ViewBag.CacheTime = DateTime.Now;
            return View("Index");
        }
    }
}