﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tryOutputCatch.Controllers
{
    public class EasyManageCacheController : Controller
    {
        [OutputCache(CacheProfile = "OneMinute")]
        public ActionResult Index()
        {
            ViewBag.Subject = "透過CacheProfile集中管理使用OneMinute的設定";
            ViewBag.Description = "當有兩個以上相同的時候都是參考相同的設定所以方便維護，例如此範例的Index與Index3";
            ViewBag.CacheTime = DateTime.Now;
            return View();
        }
        [OutputCache(CacheProfile = "TenSec")]
        public ActionResult Index2()
        {
            ViewBag.Subject = "透過CacheProfile集中管理";
            ViewBag.Description = "只Cache 10秒跟其他兩個使用OneMinute設定的不同";
            ViewBag.CacheTime = DateTime.Now;
            return View();
        }
        [OutputCache(CacheProfile = "OneMinute")]
        public ActionResult Index3()
        {
            ViewBag.Subject = "透過CacheProfile集中管理使用OneMinute的設定";
            ViewBag.Description = "當有兩個以上相同的時候都是參考相同的設定所以方便維護，例如此範例的Index與Index3";
            ViewBag.CacheTime = DateTime.Now;
            return View();
        }
    }
}