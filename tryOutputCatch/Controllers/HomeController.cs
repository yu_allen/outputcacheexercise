﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tryOutputCatch.Controllers
{
    public class HomeController : Controller
    {
        [OutputCache(Duration = 60)]
        public ActionResult Index()
        {
            ViewBag.Subject = "設定Cache時間60秒";
            ViewBag.Description = "Location 不設定的話，預設值為OutputCacheLocation.Any可參考( https://msdn.microsoft.com/zh-tw/library/dd492985(v=vs.118).aspx)";
            ViewBag.CacheTime = DateTime.Now; 
            return View();
        }

        [OutputCache(Location = System.Web.UI.OutputCacheLocation.Client,Duration = 60)]
        public ActionResult Index2()
        {
            ViewBag.Subject = "設定Cache存在Client端，時間60秒";
            ViewBag.Description = "按下F5重新整理頁面就會把存在Client的OutputCache清掉，但如果先連到別的頁面再回來此頁會發現cache還在";
            ViewBag.CacheTime = DateTime.Now;
            return View("Index");
        }
       
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.Server, Duration = 60)]
        public ActionResult Index3()
        {
            ViewBag.Subject = "設定Cache存在Server端，時間60秒";
            ViewBag.Description = "參考(https://msdn.microsoft.com/zh-tw/library/system.web.ui.outputcachelocation(v=vs.118).aspx)";
            ViewBag.CacheTime = DateTime.Now;
            return View("Index");
        }

        [OutputCache(Location = System.Web.UI.OutputCacheLocation.ServerAndClient, Duration = 60)]
        public ActionResult Index4()
        {
            ViewBag.Subject = "設定Cache存在Client端及Server端，時間60秒";
            ViewBag.Description = "參考(https://msdn.microsoft.com/zh-tw/library/system.web.ui.outputcachelocation(v=vs.118).aspx)";
            ViewBag.CacheTime = DateTime.Now;
            return View("Index");
        }
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.Any, Duration = 60)]
        public ActionResult Index5()
        {
            ViewBag.Subject = "設定Cache存在Client端及Server端及Proxy代理伺服器，時間60秒";
            ViewBag.Description = "參考(https://msdn.microsoft.com/zh-tw/library/system.web.ui.outputcachelocation(v=vs.118).aspx)";
            ViewBag.CacheTime = DateTime.Now;
            return View("Index");
        }
    }
}